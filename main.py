#Pavlo Samkov 
print('=========  =                     =========                                   =========                       ')
print('    |           _______              |       _______     _______                 |      _______     _______  ')
print('    |      |   |                     |      |       |   |                        |     |       |   |       | ')
print('    |      |   |                     |      |       |   |                        |     |       |   |       | ')
print('    |      |   |           ____      |      |_______|   |            ____        |     |       |   |_______| ')
print('    |      |   |                     |      |       |   |                        |     |       |   |         ')
print('    |      |   |_______              |      |       |   |_______                 |     |_______|   |_______  ')


import os
clear = lambda: os.system('clear')      												#for cleaning cmd 

def drawboard(board):
																					#this function draw our board 
	print("/------------------\\")
	print("|     |      |     |")
	print("|  "+board[0]+"  |  "+board[1] +"   |  "+ board[2]+"  |")
	print("|_____|______|_____|")
	print("|     |      |     |")
	print("|  "+board[3]+"  |  "+board[4] +"   |  "+ board[5]+"  |")
	print("|_____|______|_____|")
	print("|     |      |     |")
	print("|  "+board[6]+"  |  "+board[7] +"   |  "+ board[8]+"  |")
	print("|     |      |     |")
	print("\\------------------/")

	print("**********************")
																					#Here we have a hint to choosing a field
	print("How to choose")
	print(" 0 | 1 | 2 ")
	print(" ----------")
	print(" 3 | 4 | 5 ")
	print("-----------")
	print(" 6 | 7 | 8 ")

def check_win(board, mark):
																					#This function check win or tie 
	if board[0] == board[1] == board[2] == mark:									#1row
		return 1
	if board[3] == board[4] == board[5] == mark:									#2row
		return 1
	if board[6] == board[7] == board[8] == mark:									#3row
		return 1

	if board[0] == board[3] == board[6] == mark:									#1column
		return 1	
	if board[1] == board[4] == board[7] == mark:									#2column
		return 1
	if board[2] == board[5] == board[8] == mark:									#3column
		return 1


	if board[0] == board[4] == board[8] == mark:									#1diagonal
		return 1
	if board[2] == board[4] == board[6] == mark:									#2diagonal
		return 1


	if (board[0] and board[1] and board[2] and board[3] and board[4] and board[5] and board[6] and board[7] and board[8]) == ('X' or 'O'):
		return 2


	

	else:
		return 0

def start_game(player_X, player_O):
	board = [' ',' ',' ', 															#Representation of board as a list 
			' ',' ',' ',
			' ',' ',' ']
	turn = True 																	#True is X player, False is O player 
	winner = 0  																	#helper variable to control loop
	clear()     																	#clear our cmd 
	while(winner != 1 and winner != 2):										#1 - We have winner, 0 - continue the game, 2 - Tie 
		drawboard(board)				
		if turn:
			while(True):
				try:
					num = int(input(player_X + " your turn(X). Choose : "))     	#Here we choose a field 
				except ValueError:
					print("Choose CORRECT option")
					continue

				while(not num in range (0,9) or board[num] != ' '):				#Check this field is empty
					num = int(input(player_X + " Choose another field : "))			#if not choose again
				mark = 'X'															#mark X or O 
				break

		else:
			while(True):
				try:
	   				num = int(input(player_O + " your turn(O). Choose : ")) 
				except ValueError:
	   				print("Choose CORRECT option")
	   				continue
				
				while(not num in range (0,9) or board[num] != ' '):
					num = int(input(player_O + " Choose another field : "))
				mark = 'O'
				break

		board[num] = mark															#Here we use our mark and num to fill our field
		winner = check_win(board,mark)												#check someone win the game 
		turn = not turn 															#to change turn of player X=>O=>X....
		
		clear()
	if winner == 1:
		drawboard(board)
		print("************************")
		print(player_O if turn else player_X + " have won the game!!!")
		print("************************")
	elif winner == 2:
		drawboard(board)
		print("************************")
		print("!!!DRAW!!!")
		print("************************")




player_X = input("Player_1, enter your name: ")
print("Hello " + player_X + ". You will play X")
player_O = input("Player_2, enter your name: ")
print("Hello " + player_O + ". You will play O")

while(True):
	print("1.New Game")
	print("2.Exit")
	try:
   		x = int(input("Choose option: "))
	except ValueError:
   		print("Choose CORRECT option")
   		continue
	
	if x == 1:
		start_game(player_X,player_O)												#our main function 
	elif x == 2:
		break
	else:
		print("Choose CORRECT option")
	
print("Good Bye!!!")
